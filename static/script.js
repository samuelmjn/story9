$(document).ready(function(){
    let darkMode = false
    $(".button-up").click(function(){
        let parentElement = $(this).parent().parent().parent()
        let parentID = parentElement.attr('id')
        let parentIdx = parseInt(parentID.split('-')[1])

        if(parentIdx > 1) {
            let upperIdx = parentIdx-1
            let upperID = 'panel-'+upperIdx
            let upperElement = $('#' + upperID );

            // Step: copy current and upper element, then set upper element as current element vice versa
            let _upperElement = upperElement.clone(true, true)
            _upperElement.attr('id', parentID)
            let _parentElement = parentElement.clone(true, true)
            _parentElement.attr('id', upperID)

            parentElement.replaceWith(_upperElement)
            upperElement.replaceWith(_parentElement)

        }

    });

    $(".button-down").click(function(){
        let parentElement = $(this).parent().parent().parent()
        let parentID = parentElement.attr('id')
        let parentIdx = parseInt(parentID.split('-')[1])

        let divCount = 4 // TODO: Refactor this to exactly get count from parent children
        if(parentIdx < divCount) {
            let upperIdx = parentIdx+1
            let upperID = 'panel-'+upperIdx
            let upperElement = $('#' + upperID );

            // Step: copy current and upper element, then set upper element as current element vice versa
            let _upperElement = upperElement.clone(true, true)
            _upperElement.attr('id', parentID)
            let _parentElement = parentElement.clone(true, true)
            _parentElement.attr('id', upperID)

            parentElement.replaceWith(_upperElement)
            upperElement.replaceWith(_parentElement)

        }
    });

    $("#mode-switch").click(function(){
        let element = document.body
        let btnUp = $('.button-up')
        let btnDown = $('.button-down')
        element.classList.toggle("dark-mode")
        if(!darkMode) {
            darkMode = true;
            btnUp.addClass('btn-danger')
            btnDown.addClass('btn-danger')
        }
        else {
            darkMode = false;
            btnUp.removeClass('btn-danger')
            btnDown.removeClass('btn-danger')
        }
    });
});