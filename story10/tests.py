from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import register, login, home, logout
from .models import ExtUser
from django.contrib.auth.models import User

# Create your tests here.
class story10UnitTest(TestCase):
    def test_register_url_is_exist(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)

    def test_register_using_certain_template(self):
        response = Client().get('/register/')
        self.assertTemplateUsed(response, 'register.html')

    def test_register_using_certain_function(self):
        found = resolve('/register/')
        self.assertEqual(found.func, register)

    def test_login_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_login_using_certain_template(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')

    def test_login_using_certain_function(self):
        found = resolve('/login/')
        self.assertEqual(found.func, login)

    def test_home_url_is_exist(self):
        response = Client().get('/home/')
        self.assertEqual(response.status_code, 302)

    def test_home_using_certain_template(self):
        response = Client().get('/home/')
        self.assertTemplateNotUsed(response, 'home.html')

    def test_home_using_certain_function(self):
        found = resolve('/home/')
        self.assertEqual(found.func, home)

    def test_logout_is_exist(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 302)

    def test_logout_using_certain_function(self):
        found = resolve('/logout/')
        self.assertEqual(found.func, logout)

    def test_using_new_extended_users(self):
        user = User(username='haha', password='hihi')
        user.save()
        ext_user = ExtUser(user=user, profile_picture_url='haha')
        ext_user.save()
        self.assertEqual(ExtUser.objects.count(), 1)