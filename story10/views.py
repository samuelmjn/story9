from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from .models import ExtUser

# Create your views here.
def register(request):
    if request.method == 'POST':
        user = User.objects.create_user(username=request.POST.get('username'), password=request.POST.get('password'))
        user.save()
        ext_user = ExtUser(user=user, profile_picture_url=request.POST.get('profile_url'))
        ext_user.save()
        return HttpResponseRedirect('/login')

    return render(request, 'register.html')

def login(request):
    if request.session.get('user') is not None:
        return HttpResponseRedirect('/home')

    if request.method == 'POST':
        user = authenticate(request, username=request.POST.get('username'), password=request.POST.get('password'))
        if user is not None and user.is_active:
            auth_login(request, user)
            request.session['user'] = user.username
            ext_user = ExtUser.objects.get(user=user)
            request.session['profile_url'] = ext_user.profile_picture_url
            return HttpResponseRedirect('/home')
        else:
            return HttpResponseRedirect('/login')
        
    return render(request, 'login.html')

def logout(request):
    if request.session.get('user') is not None:
        del request.session['user']
    return HttpResponseRedirect('/login')

@login_required
def home(request):
    ctx = {}
    ctx['user'] = request.session['user']
    ctx['profile_url'] = request.session['profile_url']
    return render(request, 'home.html', ctx)