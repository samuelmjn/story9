from django.shortcuts import render
from django.http import JsonResponse, HttpResponseNotAllowed, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .models import BookLike, LikeRequestResponse
import json

# Create your views here.
def index(request):
    return render(request, 'accordion.html')

def book(request):
    return render(request, 'book.html')

@csrf_exempt
def like(request):
    if request.method == 'POST':
        book_id = json.loads(request.body)["book_id"]
        try:
            book = BookLike.objects.get(book_id=book_id)
            existing_book_count = book.count
            book.count = existing_book_count + 1
            book.save()

            return JsonResponse(LikeRequestResponse(book.book_id, book.count).__dict__, safe=False)

        except BookLike.DoesNotExist:
            book = BookLike(book_id=book_id)
            book.save()

            return JsonResponse(LikeRequestResponse(book.book_id, book.count).__dict__, safe=False)

    return HttpResponse(status=405)

def mostliked(request):
    books = BookLike.objects.order_by('-count')[:5]
    result = {"data": []}
    for book in books:
        result["data"].append(LikeRequestResponse(book_id=book.book_id, count=book.count).__dict__)

    return JsonResponse(result, safe=False)