from django.test import TestCase, Client, LiveServerTestCase
from .views import book, like, mostliked
from django.urls import resolve
from .models import BookLike

# Create your tests here.
class story9UnitTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/book/')
        self.assertEqual(response.status_code, 200)

    def test_using_certain_template(self):
        response = Client().get('/book/')
        self.assertTemplateUsed(response, 'book.html')

    def test_using_certain_function(self):
        found = resolve('/book/')
        self.assertEqual(found.func, book)

    def can_like_book(self):
        book_like = BookLike(book_id='testBook')
        book_like.save()
        self.assertEqual(BookLike.objects.count(), 1)

    def test_method_not_allowed_when_hitting_like_endpoint_with_get_method(self):
        response = Client().get('/like/')
        self.assertEqual(response.status_code, 405)

    def test_using_certain_function(self):
        found = resolve('/like/')
        self.assertEqual(found.func, like)

    def test_code_200_when_find_popular_book(self):
        response = Client().get('/mostliked/')
        self.assertEqual(response.status_code, 200)

    def test_using_certain_function(self):
        found = resolve('/mostliked/')
        self.assertEqual(found.func, mostliked) 