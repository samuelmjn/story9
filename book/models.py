from django.db import models

# Create your models here.
class BookLike(models.Model):
    book_id = models.CharField(max_length=20)
    count = models.BigIntegerField(default=1)

class LikeRequestResponse():
    def __init__(self, book_id: str, count: int):
        self.book_id = book_id
        self.count = count