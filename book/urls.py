from django.urls import path
from .views import index, book, like, mostliked

urlpatterns = [
    path('accordion/', index, name='index'),
    path('book/', book, name='book'),
    path('like/', like, name='like'),
    path('mostliked/', mostliked, name='mostliked'),
]